# Vault One-Time SSH Passwords

## Assumptions:
- Vault is installed on the client machine
- Vault server is configured
- ssh-pass is installed on the client machine
- Vault client is authenticated with Vault Server

## Steps:
 #### 1. Spin up a fresh instance
 #### 2. SSH on to the instance and configure the following:
- Download and extract the [vault-ssh-helper](https://releases.hashicorp.com/vault-ssh-helper/0.2.1/vault-ssh-helper_0.2.1_linux_amd64.zip) to /usr/local/bin
-	Mount the SSH engine (locally):

		  vault secrets enable ssh
	    
- mkdir -p /etc/vault-ssh-helper.d/config.hcl

	    vault_addr = "$VAULT_URL"
	    ssh_mount_point = "ssh"
	    tls_skip_verify = false
	    allowed_roles = "*"

- Test the config: 

	    vault-ssh-helper -verify-only -config=/etc/vault-ssh-helper.d/config.hcl

- Modify /etc/pam.d/sshd

		#@include common-auth
		auth requisite pam_exec.so quiet expose_authtok log=/tmp/vaultssh.log /usr/local/bin/vault-ssh-helper -config=/etc/vault-ssh-helper.d/config.hcl
		auth optional pam_unix.so not_set_pass use_first_pass nodelay


 - Modify /etc/ssh/sshd_config:
 
		ChallengeResponseAuthentication yes
		UsePAM yes
		PasswordAuthentication no
 

- Finally, create a test user: 

	    sudo useradd testuser

#### 3. Configure vault SSH Engine:


- Create a role:

	    vault write ssh/roles/otp_key_role key_type=otp default_user=testuser cidr_list=$INSTANCE_IP/32
	
- SSH to the server using vaults OTP:

	    vault ssh -role otp_key_role -mode otp testuser@$INSTANCE_IP
